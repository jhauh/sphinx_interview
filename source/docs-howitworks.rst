How it works
============

All of the magic around generating multiple different documents from the
same base RST file is provided by Sphinx. Specifically, the ``only``
directive:

.. code-block:: ReST

  .. python-questions.rst

  #. What is the difference between a list and a tuple?   

   .. only:: answers

      :Answer: 
          "Immutability" gets the mark. They may also mention better 
          performance of tuples due to simpler structure.

This command tells Sphinx to include the content defined inside the 
``only`` block if the tag ``answers`` is passed as a build parameter.
If we compare the recipes for ``make questions`` and ``make answers``:

.. code-block:: Makefile

  # Makefile

  questions:
	sphinx-build -b html source public/questions source/python-questions.rst

  answers:
	sphinx-build -b html -t answers source public/answers source/python-questions.rst

We see that the only difference in the two build commands is the 
addition of the ``answers`` tag.

The other piece of magic is in how the repo documentation HTML pages
(including this one) only show up in the docs, but not in the question
and answer documents. This is achieved through the use of the 
``exclude_patterns`` variable in ``conf.py``:

.. code-block:: Python

  # conf.py

  # Ignore source files beginning with 'docs' unless building the docs
  if tags.has('docs'):
      exclude_patterns = []
  else:
      exclude_patterns = ['docs*']

.. code-block:: ReST

  .. index.rst

  .. toctree::
     :maxdepth: 3
     :caption: Contents:

     docs-examples.rst
     docs-usage.rst
     docs-howitworks.rst
     docs-todo.rst
     python-questions.rst

.. code-block:: Makefile

  # Makefile

  docs:
	@$(SPHINXBUILD) -b html -t docs "$(SOURCEDIR)" "$(BUILDDIR)"/docs

Note that when running targets other than ``docs``, ``sphinx-build``
will warn that documents in the toctree are being ignored:

.. code-block:: Bash

  .../sphinx_interview $ make html
  Running Sphinx v3.0.3
  making output directory... done
  building [mo]: targets for 0 po files that are out of date
  building [html]: targets for 2 source files that are out of date
  updating environment: [new config] 2 added, 0 changed, 0 removed
  reading sources... [100%] python-questions
  sphinx_interview/source/index.rst:4: WARNING: toctree contains reference to excluded document 'docs-examples'
  sphinx_interview/source/index.rst:4: WARNING: toctree contains reference to excluded document 'docs-usage'
  sphinx_interview/source/index.rst:4: WARNING: toctree contains reference to excluded document 'docs-howitworks'
  sphinx_interview/source/index.rst:4: WARNING: toctree contains reference to excluded document 'docs-todo'
  looking for now-outdated files... none found
  pickling environment... done
  checking consistency... done
  preparing documents... done
  writing output... [100%] python-questions
  generating indices...  genindexdone
  writing additional pages...  searchdone
  copying static files... ... done
  copying extra files... done
  dumping search index in English (code: en)... done
  dumping object inventory... done
  build succeeded, 4 warnings.

Since this behaviour is intentional, these warnings can be safely
ignored.
