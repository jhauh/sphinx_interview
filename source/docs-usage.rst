Usage
=====

How do I generate the interview documents?
------------------------------------------

#. Clone the repo
#. Clone the environment defined in ``pyproject.toml`` 
   (really all you need is ``python3+``, ``sphinx``, and 
   ``sphinx_rtd_theme``)
#. Three output formats are currently catered for:
   
   - **HTML**: Run ``make questions`` to generate the question HTML, 
     ``make answers`` to generate the answer HTML, or 
     ``make interview`` to do both.
   - **Single page HTML**: Some may prefer to use their browser's "Print
     to PDF" functionality rather than compile to Latex/PDF directly 
     (e.g. if Latex is not installed). To generate a single page HTML
     suitable for printing, run ``make questionssinglehtml``, 
     ``make answerssinglehtml``, or
     ``make interviewsinglehtml`` as desired.
   - **PDF**: Targets are ``make questionspdf``, ``make answerspdf``,
     and ``make interviewpdf``. Uses Xelatex by default, with DejaVu
     fonts.


I want to change the interview, how?
------------------------------------

#. Clone the repo
#. Clone the environment defined in ``pyproject.toml`` 
   (really all you need is ``python3+``, ``sphinx``, and 
   ``sphinx_rtd_theme``)
#. Make your changes to ``source/python-questions.rst`` (or even add new 
   sections as new ``rst`` files, and add them to ``source/index.rst``!)
#. Run ``make <target>`` as per the above section to generate your
   prefered output format.
#. Optionally, contribute back! Submit a pull request.
#. The commit hash will be included in the produced documents, so it is
   easy to know which version of the interview a candidate took.
   Documents built on top of uncommited changes will bear the hash of
   their parent commit, suffixed by a '\*' to indicate unknown changes.
   By default it will be included in the footer of HTML outputs (see
   below) and the cover page of PDFs.

I want to change this documentation, how?
-----------------------------------------

#. Clone the repo
#. Clone the environment defined in ``pyproject.toml`` 
   (really all you need is ``python3+``, ``sphinx``, and 
   ``sphinx_rtd_theme``)
#. Make your changes to the files in ``source/`` (or even add new files
   sections as new ``docs-*.rst`` files, and add them to 
   ``source/index.rst``!)
#. Run ``make docs`` to generate the new documentation. Commit if happy.
