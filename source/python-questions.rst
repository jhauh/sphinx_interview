Python Interview Questions
==========================

.. only:: answers

   This document is designed to test the candidate's familiarity and
   working knowledge of Python. It broadly covers many aspects of the
   language's syntax and structure, and aims to give the interviewer a
   fair measurement of the candidate's experience. 
   
   It does not attempt to measure intelligence or mental agilty through
   the medium of Python quiz questions, and should not be used as such.
   It's much more of a "if you know, you know"-style test. If a
   candidate is struggling with a question, I would encourage them (or
   the interviewer) to move on to another, to show us what they do know,
   instead of trying to figure out or guess at what they don't.
   
   Marks are given as one-mark-per-question for the most part, and can
   be weighted by the difficulty section (e.g. one mark in the easy
   section is worth less than one in the difficult). This is flexible
   and left up to the interviewer to judge based on the requirements of
   the specific role. It is recommended that the relative weights are
   determined and set ahead of time, to ensure a fair comparison across
   candidates.

   It is also important to note that the answers provided here are not 
   exhaustive; there are undoubtedly many correct answers to some of the
   more open questions. The given answers should be used as a guideline
   for the "topic" of each question, and not as a hard mark scheme. 

   With this said, it is important that *at least one interviewer has 
   experience with Python*. They do not need to be an expert, or be able
   to score 100% on this section, but they do need to be able to
   validate code written, spot common errors, and likely validate code
   written by the candidate, either during the interview or with the
   code provided by the candidate afterwards.


General
-------

WCT = Write code to

Section 1
+++++++++


#. What is the difference between a list and a tuple?   
   
   .. only:: answers
      
      :Answer: 
          "Immutability" gets the mark. They may also mention better 
          performance of tuples due to simpler structure.

#. WCT print the last ten days (from now) as a list of timestamps.
   
   .. only:: answers
      
      :Answer:
          
          ::

              from datetime import date, timedelta
              today = datetime.date.today()
              last_ten_timestamps = [today - timedelta(days = i) for i in range(-9, 1)]
              print(last_ten_timestamps)
#. What are the values of ``L0``, ``L1``, ``L2``, ``L3`` in the code below::

    L0 = dict(zip(('A', 'B', 'C', 'D', 'E'), (1, 2, 3, 4, 5)))
    L1 = range(10)
    L2 = sorted([L0[s] for s in L0])
    L3 = sorted([i for i in L1 if i in L0])
  
   .. only:: answers
      
      :Answer:

          ::
              
              L0 = {'a': 1, 'c': 3, 'b': 2, 'e': 5, 'd': 4}  # Order may vary
              L1 = range(0, 10) # Or [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] in py2
              L2 = []
              L3 = [1, 2, 3, 4, 5]
#. How would you slice ``A`` to obtain ``B``? (Replace "?" with the relevant 
   code)::

    A = [[1, 2, 3, 4], [5, 6, 7, 8], ['a', 'b', 'c', 'd']]
    B = ?
    print(B)
    >>> [[3, 4], [5, 7], ['d', 'b']]

   .. only:: answers
      
      :Answer:

          ::

              B = [A[0][2:], A[1][::2], A[2][::-2]]
#. Write a unit test(s) to check the output of this function is a postive 
   integer in your preferred testing framework::

    def black_box_function(some_input):
        output = mysterious_operations(some_input)
        return output

   .. only:: answers
      
      :Answer:

          ::

              def test_black_box_function():
                  # whatever
                  assert actual == expected
#. Rewrite the code below as a list comprehension::

    values = [1, 4, 7, 10, 13]
    # rewrite the following:
    output = []
    for value in values:
        if value % 2 is 0:
            output.append(value**3 + 1)

   .. only:: answers
      
      :Answer:

          ::

              output = [value**3 + 1 for value in values if value % 2 is 0]
#. Adapt your answer to the following::

    inputs = [1, 4, 7, 10, 13]
    # rewrite the following:
    output = []
    for value in inputs:
        if value % 2 is 0:
            output.append(value**3 + 1)
        else:
            output.append(value)

   .. only:: answers
      
      :Answer:

          ::

              output = [value**3 + 1  if value % 2 is 0 else value for value in values]
#. Write a dictionary comprehension to construct a dictionary of products and 
   their prices per dozen from the following variables::

    products = ['banana', 'orange', 'apple', 'grape']
    prices = [5, 3, 4, 1]
    price_per_dozen = ?

   .. only:: answers
      
      :Answer:

          ::

              prices_per_dozen = {prod:price*12 for prod, price in zip(products, prices)}
#. WCT save the dictionary below to the file format of your choice, so it can 
   be read in by another python program::

    my_dictionary = {'firstname': 'Mickey',
                     'lastname': 'Mouse',
                     'age': 131}

   .. only:: answers
      
      :Answer:
          This can be done a couple of different ways, the main ones being 
          JSON and pickling. Either is fine, and the interviewer is free to 
          ask why the candidate chose one over the other. What we don't want
          to see is conversion to a string, or wrangling into a .csv etc.

          ::
              
              import json
              with open('my_dictionary.json', 'w') as outfile:
                  json.dump(my_dictionary, outfile)
              # Or
              import pickle
              with open('my_dictionary.pickle', 'wb') as outfile:
                  pickle.dump(my_dictionary, outfile)


Section 2
+++++++++

#. WCT construct a function which accepts a variable number of named arguments, 
   and prints their names and values.

   .. only:: answers
      
      :Answer:

          ::

              def argument_printer(**kwargs):
                  for name, value in kwargs.items(): # or iteritems in py2
                      print('the value of {0} is {1}'.format(name, value))
#. WCT construct a decorator which alters functions to print their arguments 
   before execution. Apply this decorator to a dummy function named 
   ``my_function``.

   .. only:: answers
      
      :Answer:

          ::

              def argument_printer(func):
                  def wrapper(*args, **kwargs):
                      # logic in here
                      return func(*args, **kwargs)
                  return wrapper

              @argument_printer
              def my_function(*args, **kwargs):
                  return True
#. Describe briefly how you would create (structure) and deploy a python 
   package internally to your organisation's data scientists.

   .. only:: answers
      
      :Answer:
          Two marks for this one, one for create, one for deploy. Half marks 
          may be awarded, i.e. a candidate can get 1/2 mark in each section.

          :Create: Demonstrates knowledge of ``pip``, ``conda``, 
                   ``setuptools``, or any other relevant packaging tools. 
                   In terms of structuring, any kind of folder structure,
                   import hierarchy, init files, dependencies or mentions of 
                   ``setup.py`` etc. are useful.

          :Deploy: Upload to internal package repository, host on VCS (e.g. 
                   GitHub) with install instructions (``pip install package``),
                   or mentions of managed software software are useful. 
                   Discussion of pros and cons of each is also useful.
#. Alter the following function so it does not fail when ``A`` is a string, 
   but instead converts ``A`` to a float, and if that cannot be done, sets the 
   value of the output to 0::

    A = 'one'
    def add_one(number):
        result = number + 1
        return result

    >>> add_one(A)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "<stdin>", line 2, in add_one
    TypeError: Can't convert 'int' object to str implicitly

   .. only:: answers
      
      :Answer:

          ::

              A = 'one'
              def add_one(number):
                  try:
                      result = number + 1
                  except TypeError:
                      try:
                          float(number) + 1
                      except:
                          result = 0
                  return result
#. WCT construct a generator function which produces Fibonnacci numbers 
   (0, 1, 1, 2, 3, 5...).

   .. only:: answers
      
      :Answer:
          This doesn't need to be perfect, but should include a solid
          initialisation and a call to yield

          ::

              def fibonnacci():
                  a , b = 0 , 1
                  while True :
                      yield a
                      a , b = b , a + b                  
#. WCT construct a ``Rectangle`` class which extends the ``Polygon`` class 
   below, adding an ``area`` attribute which is calculated by a new ``findArea``
   method. Include a sanity check which raises an error if ``no_of_sides``
   is not 4 when the class is instantiated::

    class Polygon(object):
        def __init__(self, no_of_sides):
            self.n = no_of_sides
            self.side_lengths = [0 for i in range(no_of_sides)]

        def inputSides(self):
            self.side_lengths = [float(input("Enter side "+str(i+1)+" : ")) \
                 for i in range(self.n)]
    
        def displaySides(self):
            for i in range(self.n):
                print("Side",i+1,"is",self.side_lengths[i])

   .. only:: answers
      
      :Answer:

          ::

              class Rectangle(Polygon):
                  def __init__(self, no_of_sides):
                      super.__init__(self, no_of_sides)
                      if self.n != 4:
                          raise ValueError('Rectangles have 4 sides, not ' + self.n)

                  def findArea(self):
                      if self.side_lengths == [0, 0, 0, 0]:
                          raise ValueError('Input side lengths before calculating area') 
                      # check for two pairs of equal length sides
                      unique_sides = set(self.side_lengths) 
                      if len(unique_sides) is 2:
                          length, width = unique_sides
                      elif len(unique_sides) is 1:
                          length = width = unique_sides
                      else:
                          raise ValueError('This quadrilateral is not a rectangle')
                      self.area = length * width


Section 3
+++++++++

#. Describe the overall function of the (unfortunately uncommented) code below. 
   Qualitatively, explain what ``B`` represents::

    import random as r
    import math as m
    
    i = 0
    N = 1000000
    
    for a in range(0, N):
      x2 = r.random()**2
      y2 = r.random()**2
        if m.sqrt(x2 + y2) < 1.0:
          i += 1
    
    B = (float(i) / N) * 4
    print(B)

   .. only:: answers
      
      :Answer:
          B is an approximation of pi
#. If we list all the natural numbers below 10 that are multiples of 3 or 5, 
   we get 3, 5, 6 and 9. The sum of these multiples is 23.
   
   WCT find the sum of all the multiples of 3 or 5 below 1000.
   
   .. only:: answers
      
      :Answer:
          This is the first Project Euler question. There are loads of ways to
          do it, but my favourite two are:

          ::

              def multiples_of_3_or_5():
                  for number in xrange(1000):
                      if not number % 3 or not number % 5:
                          yield number
        
              print sum(multiples_of_3_or_5())
              # or
              print sum(number for number in xrange(1000) if not (number % 3 and number % 5))


.. Section 4
.. +++++++++

.. #. (python 3.6+) Add type hinting to the following code::
.. 
..     from datetime import date
.. 
..     class Birthday(name, age, birthdate):
..         def __init__(self, name, age, birthdate):
..             self.name = name
..             self.age = age
..             self.birthdate = birthdate
.. 
..         def is_birthday(self):
..             t = date.today()
..             return (t.month, t.day) == (self.birthdate.month, self.birthdate.day)
.. 
..         def countdown(self):
..             t = date.today()
..             days_this_year = date(t.year, self.birthdate.month, self.birthdate.day) - t
..             days_next_year = date(t.year + 1, self.birthdate.month, 
..               self.birthdate.day) - t
..             days_left = min(days_this_year, days_next_year).days \
..                 if days_this_year.days > 0 else days_next_year.days
..             print(self.name + ', you will be in ' + str(days_left) + ' days! Woohoo!')
..             return days_left
.. 
..    .. only:: answers
..       
..       :Answer:
.. 
..           ::
                

Pandas
------

Section 1
+++++++++
 
#. WCT create a pandas DataFrame with 2 columns: one column with natural 
   numbers from 1 to 100 and the second column with a character representation 
   of the same numbers, i.e. “1”, “2”, …, “100”.

   .. only:: answers
      
      :Answer:
          Again, lots of options, but something like:

          ::

              df = pd.DataFrame({'a':range(1, 101), 
                                 'b': [str(x) for x in range(1, 101)]})
#. WCT create a pandas DataFrame with a tabular data from the following TSV 
   file named ``library1.txt``::
    
    BookID;LoanDate;LoanLength;ReaderID
    aaaa;2018/09/27;2018/10/04;1234
    aabb;2018/10/04;2018/10/28;1234
    bbca;2018/09/01;2018/10/01;5678

   .. only:: answers
      
      :Answer:
          Test-the-waters question. If they can't do this, don't ask any more
          pandas questions.

          ::

              df = pd.read_csv('library1.txt', sep = ';')
#. Extend your answer to read many files organised into daily folders as per 
   below into a single pandas DataFrame::

    .
    └── 07
        ├── 01
        │   ├── library1.txt
        │   ├── library2.txt
        │   └── library3.txt
        └── 02
            ├── library1.txt
            ├── library2.txt
            └── library3.txt
   
   .. only:: answers
      
      :Answer:
          Looking for use of the ``glob`` module, or ``os``. Any answer 
          defining all the filepaths above in a list is limited to 1/2 mark. 
          One possible solution is:

          ::
              
              import glob
              all_files = glob.glob('./07/**.txt') # or use os for filepath
              df = pd.concat((pd.read_csv(f) for f in all_files))
#. How would you estimate the frequencies of a character variable in a pandas 
   data frame?
   
   .. only:: answers
      
      :Answer:
          ``pandas.Series.value_counts``
#. A data frame has columns named like “c.1”, “c.2”, …, “c.1000” WCT rename the 
   100 last columns in the pandas data frame to “c.901.par”, …, “c.1000.par”?

   .. only:: answers
      
      :Answer:
          The *last 100* part of this question makes it less straightforward.
          Award 1/2 mark to answers adding ".par" to all columns.
          
          ::
              
              replacements = {col: col + '.par' for col in df.columns[-100:]}
              df.rename(index = str, columns = replacements, inplace=True)   
#. A pandas data frame has a character column with dates formatted like 
   “25-Jan-2015”. WCT sort the data frame by these dates in ascending order.

   .. only:: answers
      
      :Answer:

          ::

              df.assign('Date', lambda x pd.to_datetime(x['Date'])) \
                .sort('Date')
#. Aggregate the following pandas DataFrame to the month level, to obtain the 
   total spend per month:
   
  =====  ===  =====
  Month  Day  Spend
  =====  ===  =====
  01     30   32.50
  01     31   24.20
  02     01   19.40
  02     02   42.30
  ...
  =====  ===  =====

   .. only:: answers
      
      :Answer:
          There are a few variations on the syntax, but something like:

          ::

              df.groupby('Month')['Spend'].agg(sum)

Section 2
+++++++++

#. Rewrite the following code to improve performance, making use of vectorized 
   functions/methods::
    
    import numpy as np

    def get_distance(x, y):
        ''' grid is a 10 x 10 unit square, so (5, 5) is the centre'''
        return np.sqrt((x - 5)**2 + (y - 5)**2)

    coords['distance_from_centre'] = \
        coords.apply(lambda point: get_distance(point['x'], point['y']), axis = 1)

   .. only:: answers
      
      :Answer:
          Vectorization is a good topic to separate beginner and intermediate 
          pandas users. There are a couple of ways to answer this question,
          depending on whether or not the candidate realises the involved 
          operators and methods are already able to operate on arrays. The simplest approach is:

          ::

              coords['distance_from_centre'] = get_distance(coords['x'], coords['y'])
#. WCT add a column to the below dataset which is the difference between the
   previous and current row's temperature values

   =========== ====
   Measurement Temp
   =========== ====
   1           21
   2           25
   3           49
   ...
   =========== ====

   .. only:: answers
      
      :Answer:
          Knowledge of ``.shift``. Full mark still awarded for a working 
          manual self-join.

          ::

              df['Temp_change'] = df['Temp'] - df['Temp'].shift(1)
#. Rewrite the following code as a single chained call::
    
    df = pd.read_csv('library1.csv')
    df['LoanDate'] = pd.to_datetime(df['LoanDate'])
    df.rename(str.lower, axis='columns', inplace=True)
    df.drop('LoanLength', axis='columns', inplace=True)

   .. only:: answers
      
      :Answer:

          ::

              df = (
                    pd.read_csv('library1.csv')
                      .assign(LoanDate = lambda x: pd.to_datetime(x['LoanDate']))
                      .rename(columns = str.lower)
                      .drop('LoanLength', axis=1)
                    )
#. Describe qualitatively how you would test the structure/value of a pandas 
   DataFrame object.

   .. only:: answers
      
      :Answer:
          Awareness of ``pandas.testing.assert_frame_equal`` gets the mark, 
          but 1/2 mark also available for any reasonably competent answer
          around more manually comparing shape, columns, types, summary
          statistics etc.        
#. WCT transform the pandas DataFrame into the shape specified below:
   
  ========= ====== ====== ======
  StudentID Exam_1 Exam_2 Exam_3
  ========= ====== ====== ======
  1         87     95     100
  2         76     64     55
  3         32     34     100
  ========= ====== ====== ======

  ========= ==== =====
  StudentID Exam Score
  ========= ==== =====
  1         1    87
  1         2    85
  1         3    100
  2         1    76
  2         2    64
  ...
  ========= ==== =====

   .. only:: answers
      
      :Answer:
          This question gets at two things: knowledge of ``melt``, and some 
          data manipulation afterwards. Working code required for 
          each part to get each 1/2 mark.

          ::

              long = pd.melt(wide, id_vars = 'StudentID', var_name = 'Exam', 
                  value_name = 'Score')
              long['Exam'] = long['Exam'].str.replace('Exam_', '')


Section 3
+++++++++

#. Slice the below MultiIndexed pandas DataFrame to obtain the location where 
   the value 99 is found::

    >>> print(df)
                         A         B         C
    first second                              
    bar   one     0.895717  0.410835 -1.413681
          two     0.805244  0.813850  1.607920
    baz   one    -1.206412  0.132003  1.024180
          two     2.565646 -0.827317        99
    foo   one     1.431256 -0.076467  0.875906
          two     1.340309 -1.187678 -2.211372
    qux   one    -1.170299  1.130127  0.974466
          two    -0.226169 -1.436737 -2.006747

    >>> A = ?

   .. only:: answers
      
      :Answer:

          ::

              A = df.loc[('baz', 'two'), 'C']

Scikit-learn
------------

Section 1
+++++++++

#. WCT save a fitted ``sklearn.linear_model.LinearRegression`` model named 
   ``best_predictor`` to disk, so it can be read in and used by another script. 
   Then, WCT read this model back in to memory.

   .. only:: answers
      
      :Answer:
          Basic understanding of ``joblib``. ``pickle`` is also fine.

          ::

              from sklearn.externals import joblib # forgive incorrect paths
              joblib.dump(best_predictor, 'best_predictor.joblib')
              
              # and then later,
              best_predictor = joblib.load('best_predictor.joblib')
#. WCT split a large dataset ``accounts`` into separate ``train`` and ``test`` 
   sets.

   .. only:: answers
      
      :Answer:
          ``sklearn.model_selection.train_test_split`` is what we are looking 
          for, but other packages providing similar functionality are fine. 
          1/2 mark for manual implementations

          ::

              from sklearn.model_selection import train_test_split
              train, test = train_test_split(accounts, random_state = 42)
#. For what purpose would you use a Vectorizer (e.g. 
   ``sklearn.feature_extraction.text.CountVectorizer``)? Briefly describe what 
   they do.

   .. only:: answers
      
      :Answer:
          The typical use case is converting a collection of unstructured 
          text documents into a structured matrix of token counts, which can 
          then be more readily fed into your estimator of choice.

          The method path given in the question is a hint, so probe for details to find out if the candiate can discuss in detail.
#. Given a model object ``final_model`` and an appropriately cleaned validation 
   dataset ``holdout_data``, WCT score the model on this data, to produce an 
   array (or column/Series/etc.) of scores.

   .. only:: answers
      
      :Answer:
          Typically something like the below, but the question is broad enough 
          that there are a number of valid answers.

          ::

              scores = final_model.transform(holdout_data)


Section 2
+++++++++

#. Briefly describe the purpose of the ``sklearn.pipeline.Pipeline`` class, 
   and give a qualitative example.

   .. only:: answers
      
      :Answer:
          "The purpose of the pipeline is to assemble several steps that can 
          be cross-validated together while setting different parameters." It 
          consists of a series of transforms with a final estimator.
#. Briefly describe how you would set up a grid search to tune the 
   hyperparameters of an Estimator.

   .. only:: answers
      
      :Answer:
          ``sklearn.model_selection.GridSearchCV`` is the main topic here, 
          ``RandomizedSearchCV`` is also fine. Looking for some depth to the 
          candidates knowledge, e.g. discussion of the required inputs 
          (estimator, parameter grid, scoring method etc.)


.. Difficult aka Section 3
.. +++++++++


TensorFlow
----------

Section 1
+++++++++

#. What is the TensorFlow library, what it is used for, and when might 
   you use it?

   .. only:: answers
      
      :Answer:
          "TensorFlow is an open source software library for numerical 
          computation using data-flow graphs."
#. Do you have experience working with TensorFlow, or any higher-level 
   abstractions which use it?

   .. only:: answers
      
      :Answer:
          If no, stop asking TF questions obviously. No marks to be awarded 
          for this question either way.
#. What is a Tensor?

   .. only:: answers
      
      :Answer:
          An n-dimensional array of primitive datatypes, where n is known as the rank of the tensor.


.. Moderate aka Section 2
.. ++++++++

.. Difficult aka Section 3
.. +++++++++


