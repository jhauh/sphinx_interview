Examples
========

All of the different outputs (including the questions page in this
documentation) are generated from the single ``python-questions.rst``
file in the ``source/`` folder of this repository. Here are some
examples of the questions vs answers files:

**HTML**:

.. Use substitution definitions to get pairs of images on the same line

|html_questions| |html_answers|

.. |html_questions| image:: _static/html_questions.png
   :width: 48%

.. |html_answers| image:: _static/html_answers.png
   :width: 48%

**PDF**:

|pdf_questions| |pdf_answers|

.. |pdf_questions| image:: _static/pdf_questions.png
   :width: 48%

.. |pdf_answers| image:: _static/pdf_answers.png
   :width: 48%
