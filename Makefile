# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SOURCEDIR     = source
BUILDDIR      = public

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile docs questions answers interview questionsinglehtml answersinglehtml interviewinglehtml questionspdf answerspdf interviewpdf

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

docs:
	@$(SPHINXBUILD) -b html -t docs "$(SOURCEDIR)" "$(BUILDDIR)"/docs

questions:
	@$(SPHINXBUILD) -b html "$(SOURCEDIR)" "$(BUILDDIR)"/questions "$(SOURCEDIR)"/python-questions.rst

answers:
	@$(SPHINXBUILD) -b html -t answers "$(SOURCEDIR)" "$(BUILDDIR)"/answers "$(SOURCEDIR)"/python-questions.rst

interview:
	make questions
	make answers

questionssinglehtml:
	@$(SPHINXBUILD) -b singlehtml "$(SOURCEDIR)" "$(BUILDDIR)"/questionssinglehtml "$(SOURCEDIR)"/python-questions.rst

answerssinglehtml:
	@$(SPHINXBUILD) -b singlehtml -t answers "$(SOURCEDIR)" "$(BUILDDIR)"/answerssinglehtml "$(SOURCEDIR)"/python-questions.rst

interviewsinglehtml:
	make questionssinglehtml
	make answerssinglehtml

questionspdf:
	@$(SPHINXBUILD) -b latex "$(SOURCEDIR)" "$(BUILDDIR)"/questionspdf "$(SOURCEDIR)"/python-questions.rst
	cd "$(BUILDDIR)"/questionspdf; make

answerspdf:
	@$(SPHINXBUILD) -b latex -t answers "$(SOURCEDIR)" "$(BUILDDIR)"/answerspdf "$(SOURCEDIR)"/python-questions.rst
	cd "$(BUILDDIR)"/answerspdf; make

interviewpdf:
	make questionspdf
	make answerspdf