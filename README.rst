Sphinx Technical Interview Template
===================================

What is this?
-------------

What better way to create a Python interview than in Python?

``sphinx`` is the Python engine which powers everything here. The
content (questions, answers) is written in a single ``rst`` file in the
``source`` folder, and then sphinx converts this into a more
professional-looking HTML, PDF, or several other formats suitable for
showing to candidates.

The example questions provided here are for a Python/Data science
interview, but this template can be used for any technical/programming
topic.

See the documentation for more info: https://jhauh.gitlab.io/sphinx_interview/


But why?
--------

There are many reasons for writing interview questions in this way:

+ The ``rst`` format is plaintext (it's a lot like markdown), so it can 
  be version controlled, so it's easy to track changes to questions, 
  additions or deletions, and do things like bring back old questions, 
  or even maintain different branches for different roles, with 
  different levels of skill requirements.
+ This also makes it very easy to track which candidate received which 
  version of the interview, which may be useful for comparing candidates
  over time.
+ The questions and answers are stored in the same file, so it's easy to
  make changes, and hard to accidentally let the questions and answers
  fall out of sync.
+ Since ``sphinx`` can output to many different formats, the ``rst``
  file serves a "ground truth" of the interview, making it easy to keep
  online interviews consistent with face-to-face ones.
+ It handles code blocks well and provides syntax highlighting
  automatically, making the included code clear to read. This is an 
  advantage over doing something like this in Word, where one would 
  spend significant time doing uninteresting formatting.
+ The interview is intended to be given by someone who knows the topic 
  at hand. All this rigmarole can arguably be seen as a 
  barrier-to-entry for would-be non-technical interviewers. Still, 
  nothing is stopping you from grabbing the pdfs and running with them.


To summarise, this approach does more to ensure the consistency of the 
interview, and handles all the annoying formatting automatically.


How do I generate the interview documents?
------------------------------------------

#. Clone the repo
#. Clone the environment defined in ``pyproject.toml`` 
   (really all you need is ``python3+``, ``sphinx``, and 
   ``sphinx_rtd_theme``)
#. Three output formats are currently catered for:
   
   - **HTML**: Run ``make questions`` to generate the question HTML, 
     ``make answers`` to generate the answer HTML, or 
     ``make interview`` to do both.
   - **Single page HTML**: Some may prefer to use their browser's "Print
     to PDF" functionality rather than compile to Latex/PDF directly 
     (e.g. if Latex is not installed). To generate a single page HTML
     suitable for printing, run ``make questionssinglehtml``, 
     ``make answerssinglehtml``, or
     ``make interviewsinglehtml`` as desired.
   - **PDF**: Targets are ``make questionspdf``, ``make answerspdf``,
     and ``make interviewpdf``. Uses Xelatex by default, with DejaVu
     fonts.


I want to change the interview, how?
------------------------------------

#. Clone the repo
#. Clone the environment defined in ``pyproject.toml`` 
   (really all you need is ``python3+``, ``sphinx``, and 
   ``sphinx_rtd_theme``)
#. Make your changes to ``source/python-questions.rst`` (or even add new 
   sections as new ``rst`` files, and add them to ``source/index.rst``!)
#. Run ``make <target>`` as per the above section to generate your
   prefered output format.
#. Optionally, contribute back! Submit a pull request.
